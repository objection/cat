#include "cat.h"
#include <errno.h>
#include <string.h>

int main (int argc, char **argv) {
	FILE *read_fs[3];
	const char *files[3] = {
		"/home/neil/.vimrc",
		"/home/neil/.zshrc",
		"/etc/passwd",
	};
	for (int i = 0; i < 3; i++) {
		read_fs[i] = fopen (files[i], "r");
		if (!read_fs[i]) {fprintf (stderr, "bad file: %s\n", strerror (errno));}
	}

	cat (stdout, read_fs, 3, CAT_FLAG_SHOW_TABS);
#if 0
	/* cat (stdout, read_fs, 3, CAT_FLAG_SHOW_TABS); */
	char *buf;
	/* cat_into_buf (&buf, read_fs, 3, CAT_FLAG_SHOW_TABS); */
	/* printf ("%s\n", buf); */
	cat_from_path_into_buf (&buf, files, 3, CAT_FLAG_SHOW_TABS);
	printf ("%s\n", buf);
	/* cat_from_path (stdout, files, 3, CAT_FLAG_SHOW_TABS); */
#endif


}
