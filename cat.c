#define _GNU_SOURCE
#include "cat.h"
#include "darr.h"
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <stdarg.h>

DECLARE_ARR (char, char);
DEFINE_ARR (char, char);

char cat_number_buf[32];

struct cat_out_funcs {
	void (*fmt) (void *, char *, ...);
	void (*_char) (int c, void *where);
};

static void cat_write_char (int c, void *where) {
	struct cat_char_arr *buf = where;
	cat_push_char_arr (buf, c);
}

static void cat_print_char (int c, void *where) {
	FILE *f = where;
	fputc (c, f);
}

static void cat_do_line (void (*func) (int c, void *where), void *out,
		char *line, size_t max_line, enum cat_flag flags) {

	for (char *p = line; *p && p - line < max_line; p++) {
		if (*p == '\n') {
			if (flags & CAT_FLAG_SHOW_ENDS) {
				func ('$', out);
			}
		}
		if (*p == '\t') {
			if (flags & CAT_FLAG_SHOW_TABS) {
				func ('^', out);
				func ('I', out);
				p++;
				if (!*p || p - line >= max_line) break;
			}
		}
		func (*p, out);
	}
}

static void write_fmt (void *char_arr, char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	sprintf (cat_number_buf, fmt, ap);
	struct cat_char_arr *buf = char_arr;
	cat_insert_char_arr (buf, cat_number_buf, (*buf).n,
			strlen (cat_number_buf));
	va_end (ap);
}

static void print_fmt (void *file, char *fmt, ...) {
	(void) file;
	va_list ap;
	va_start (ap, fmt);
	vprintf (fmt, ap);
	va_end (ap);
}

/* These are the functions used to print/write. My way of DRYing mysef.
 * It's not bad, you know. Sticking them in a structs like this keeps the
 * functions together. Then whole thing shouldn't be slow. Passing these
 * to cat_do_cat means no extra branching. */
struct cat_out_funcs print = {
	.fmt = print_fmt,
	._char = cat_print_char,
}, write = {
	.fmt = write_fmt,
	._char = cat_write_char,
};

static void cat_do_cat (struct cat_out_funcs cat_out_funcs, void *out,
		FILE **fs, size_t n_fs, enum cat_flag flags) {
	size_t n_line = 256;
	char *line = malloc (n_line);
	if (!line) assert (!"Malloc failed");

	// I could have you pass n_line but this is simpler.
	// Maybe you I could have you #define something if you want
	// different behaviour.
	size_t n_lines = 0;
	bool last_line_blank = 0;
	bool skip = 0;
	for (FILE **f = fs; f < fs + n_fs; f++) {
		while (getline (&line, &n_line, *f) != -1) {
			skip = 0;

			// Actual cat does something more complicated than this,
			// but it only means something after thousands and thousands
			// of lines.
			if (flags & CAT_FLAG_NUMBER)
				cat_out_funcs.fmt (out, "%6zu  ", n_lines);

			if (*line == '\n') {
				if (flags & CAT_FLAG_SQUEEZE_BLANK && last_line_blank)
					skip = 1;
				last_line_blank = 1;
			}

			if (!skip) {
				cat_do_line (cat_out_funcs._char, out, line, n_line, flags);
				if (*line != '\n') last_line_blank = 0;
			}
			n_lines++;
		}
	}
	free (line);
}

/* Prints contents of FILE *s fs in the manner specified by flags. */
void cat_fps (FILE *out_f, FILE **fs, size_t n_fs, enum cat_flag flags) {
	cat_do_cat (print, out_f, fs, n_fs, flags);
}

/* Writes contents of FILE *s fs into *res in the manner specified by
 * flags. */
void cat_fps_into_buf (char **res, FILE **fs, size_t n_fs,
		enum cat_flag flags) {
	struct cat_char_arr buf = cat_create_char_arr (1024);
	cat_do_cat (write, &buf, fs, n_fs, flags);
	*res = buf.d;
}

/* Makes FILE *s from in_paths, passes them to cat_do_cat */
static int cat_do_cat_from_path (
		struct cat_out_funcs cat_out_funcs,
		void *out, char **in_paths, size_t n_paths,
		enum cat_flag flags) {
	int res = 0;

	// SPEED: You could just call cat with a single FILE * repeatedly
	// rather than making this array. Dunno which'd be faster.
	FILE *fs[n_paths];
	for (int i = 0; i < n_paths; i++) {
		fs[i] = fopen (in_paths[i], "r");
		if (!fs[i]) return res = 1;
	}
	cat_do_cat (cat_out_funcs, out, fs, n_paths, flags);
	for (FILE **f = fs; f < fs + n_paths; f++)
		fclose (*f);
	return res;

}

/* Prints contents of char *s in_paths in the manner specified by flags. */
int cat_paths (FILE *out_f, char **in_paths, size_t n_paths,
		enum cat_flag flags) {
	return cat_do_cat_from_path (print, out_f, in_paths, n_paths, flags);
}

/* Writes contents of in_paths into *res in the manner specified by flags. */
int cat_paths_into_buf (char **res, char **in_paths,
		size_t n_paths, enum cat_flag flags) {
	struct cat_char_arr buf = cat_create_char_arr (1024);
	int ret = cat_do_cat_from_path (write, &buf, in_paths, n_paths, flags);
	*res = buf.d;
	return ret;
}
