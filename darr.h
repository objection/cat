
#pragma once
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAX(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _a : _b; })

#define MIN(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _b : _a; })

#define set_arr_to_x(arr, x, n) \
	for (size_t i = 0; i < (n); i++) { \
		(arr)[i] = (x); \
	}

#define DECLARE_ARR(type, name) \
		typedef struct cat_##name##_arr { \
			type *d; \
			size_t a, n; \
		} cat_##name##_arr; \
		cat_##name##_arr cat_create_##name##_arr(size_t init_size); \
		int cat_push_##name##_arr(cat_##name##_arr *s, type item); \
		int cat_insert_##name##_arr (cat_##name##_arr *s, type *what, size_t where, size_t n); \

#define DEFINE_ARR(type, name) \
		cat_##name##_arr cat_create_##name##_arr(size_t init_size) { \
			cat_##name##_arr res; \
			if (!init_size) init_size = 4; \
			res.d = malloc (sizeof(type) * init_size); \
			if (!res.d) { \
				free (res.d); \
				res.a = -1; \
			} else { \
				res.n = 0, res.a = init_size; \
			} \
			return res; \
		} \
		int cat_push_##name##_arr(cat_##name##_arr *s, type item) {\
			type *tmp; \
			if ((*s).n >= (*s).a) { \
				tmp = realloc ((*s).d, ((*s).a * 2) * sizeof (type)); \
				if (!tmp) return -1;  \
				(*s).d = tmp; \
				(*s).a *= 2; \
			} \
			(*s).d[(*s).n++] = item; \
			return (*s).n; \
		} \
		int cat_insert_##name##_arr (cat_##name##_arr *s, type *what, size_t where, \
				size_t n) { \
			if (where > (*s).n) return 1; \
			if ((*s).n + n >= (*s).a) { \
				(*s).a += n + 1;  \
				(*s).a *= 2; \
				type *tmp = realloc ((*s).d, (*s).a * sizeof *(*s).d); \
				if (!tmp) return 2; \
				(*s).d = tmp; \
			} \
			memmove ((*s).d + where + n, (*s).d + where, \
					((*s).n - where) *  sizeof *(*s).d); \
			memcpy ((*s).d + where, what, n * sizeof *(*s).d); \
			(*s).n += n; \
			return 0; \
		}
