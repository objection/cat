#if 0
Usage: cat [OPTION]... [FILE]...
Concatenate FILE(s) to standard output.

With no FILE, or when FILE is -, read standard input.

  -A, --show-all           equivalent to -vET
  -b, --number-nonblank    number nonempty output lines, overrides -n
  -e                       equivalent to -vE
  -E, --show-ends          display $ at end of each line
  -n, --number             number all output lines
  -s, --squeeze-blank      suppress repeated empty output lines
  -t                       equivalent to -vT
  -T, --show-tabs          display TAB characters as ^I
  -u                       (ignored)
  -v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB
      --help     display this help and exit
      --version  output version information and exit

Examples:
  cat f - g  Output f's contents, then standard input, then g's contents.
  cat        Copy standard input to standard output.

GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
Full documentation <https://www.gnu.org/software/coreutils/cat>
or available locally via: info '(coreutils) cat invocation'
#endif

#include <stdio.h>
#include <assert.h>
enum cat_flag {
	CAT_FLAG_NUMBER_NONBLANK = 1 << 0,
	CAT_FLAG_SHOW_ENDS = 1 << 1,
	CAT_FLAG_NUMBER = 1 << 2,
	CAT_FLAG_SQUEEZE_BLANK = 1 << 3,
	CAT_FLAG_SHOW_TABS = 1 << 4,
	// Not implemented.
	CAT_FLAG_SHOW_NONPRINTING = 1 << 5,
	// Not implemented because CAT_FLAG_SHOW_NONPRINTING not
	// implemented.
	CAT_FLAG_SHOW_ALL = 1 << 6,
	// -e: Ditto.
	CAT_FLAG_E = 1 << 7,
	// -t: Ditto.
	CAT_FLAG_T = 1 << 8,
};

#define cat(out, ins, n, flags) \
	_Generic((out), \
		FILE *: \
			_Generic((ins), \
				FILE **: cat_fps, \
				char **: cat_paths), \
		char **: \
			_Generic((ins), \
				FILE **: cat_fps_into_buf, \
				char **: cat_paths_into_buf)) \
		(out, ins, n, flags)

void cat_fps (FILE *out_f, FILE **fs, size_t n_fs, enum cat_flag flags);
int cat_paths (FILE *out_f, char **paths, size_t n_paths,
		enum cat_flag flags);
void cat_fps_into_buf (char **res, FILE **in_fs, size_t n_in_fs,
		enum cat_flag flags);
int cat_paths_into_buf (char **res, char **paths, size_t n_paths,
		enum cat_flag flags);
