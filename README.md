# Cat

## Is

This is cat as a small C library. You can give it most of cat's flags.
It will concatenate, as you'd expect, multiple files, which you can
give as FILE \*s or char \*s. You can write to a FILE \* of your
choice or a buf.

That means, since it's C, there's four functions, but I've very
cleverly used C99's generic selection, meaning so you can call "cat"
and ignore all the rest if you like.

## All For One

The four functions are:

```
	cat_fps
	cat_paths
	cat_fps_into_buf
	cat_paths_into_buf
```
The "\_fps\_" ones take FILE \*s; the "\_into\_buf\_" ones write to a
char \* you provide.

## One for All

Since I've at great pains to myself made a cat macro, you can use
cat and forget these names.

Eg:
```
	char *buf;
	size_t n_files = 2;
	char *files[n_files] = {"/etc/hosts", "/etc/passwd"};
	cat (&buf, files, n_files, CAT_FLAG_NUMBER);
```

I'm not convinced this is better. We will learn together.

## Flags

All of the cat's options excluding -v and everything related to v are
implemented. You can see them in cat.h.

## Limitations/Bugs

--number spacing is calculated in a simple way -- it should look the
same up to 6 digits. Fuck know what cat does after that.

## Install

If you want you can just include the files in your project.

You can build it as a shared library with meson:

```
meson build
sudo meson install -Cbuild --only-changed
```
